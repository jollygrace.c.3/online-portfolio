# Web Developer Portfolio - Code Review Session
Clone this project to your Batch87/S01/A1/online-portfolio folder

## Folder Structure:
- Put all your resources(HTML and CSS files) inside the public folder 
- Inside the public folder, create an assets folder and add the css and images folder inside it. 
- Inside the css folder, add an style.css file
- Add all images inside the images folder
---

## Deployment Steps: 
- https://gitlab.com/help/user/project/pages/getting_started/pages_new_project_template.md
---

## Installation Checks
- git
	command: git --version

- node 
	command : node -v

- npm 
	command: npm -v

---

## Basic CLI commands

ls
	- checks the list ofitems inside the directory

cd
	- change directory

cd ..
	- one folder up/ go back to the parent folder

pwd
	- check the current location in the system

mkdir <folder_name>
	- creates a folder/directory

touch <file_name>
	- creates a file